"""
Python list model
"""
from datetime import date
from .Model import Model

class model(Model):
    def __init__(self):
        self.charities = []

    def select(self):
        """
        Returns charities list of lists
        Each list in charities contains: name, description, address, website, phone
        :return: List of lists
        """
        return self.charities

    def insert(self, name, description, address, website, phone):
        """
        Appends a new list of values representing new charity into charities
        :param name: String
        :param description: String
        :param address: String
        :param website: String
        :param phone: String
        :return: True
        """
        params = [name, description, address, website, phone]
        self.charities.append(params)
        return True
