from flask import render_template
from flask.views import MethodView
import gbmodel

class Charities(MethodView):
    def get(self):
        model = gbmodel.get_model()
        entries = [dict(name=row[0], description=row[1], address=row[2], website=row[3], phone=row[4] ) for row in model.select()]
        return render_template('charities.html',entries=entries)
