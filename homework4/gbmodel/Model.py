class Model():
    def select(self):
        """
        Gets all entries from the database
        :return: Tuple containing all rows of database
        """
        pass

    def insert(self, name, description, address, website, phone):
        """
        Inserts entry into database
        :param name: String
        :param description: String
        :param address: String
        :param website: String
        :param phone: String
        :return: none
        :raises: Database errors on connection and insertion
        """
        pass
