"""
A simple charity database flask app.
ata is stored in a SQLite database that looks something like the following:

+-------------------+-------------+-------------+-------------+--------------+
| Name              | Description | Address     | Website     | Phone        |
+===================+=============+=============+=============+==============+
| Hope Food Pantry  | Food pantry | 123 Main St | example.com | 123-456-7890 |
+-------------------+-------------+-------------+-------------+--------------+

This can be created with the following SQL (see bottom of this file):

    create table charity (name text, description text, address text, website text, phone text);

"""
from datetime import date
from .Model import Model
import sqlite3
DB_FILE = 'entries.db'    # file for our Database

class model(Model):
    def __init__(self):
        # Make sure our database exists
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        try:
            cursor.execute("select count(rowid) from charity")
        except sqlite3.OperationalError:
            cursor.execute("create table charity (name text, description text, address text, website text, phone text)")
        cursor.close()

    def select(self):
        """
        Gets all rows from the database
        Each row contains: name, description, address, website, phone
        :return: List of lists containing all rows of database
        """
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM charity")
        return cursor.fetchall()

    def insert(self, name, description, address, website, phone):
        """
        Inserts entry into database
        :param name: String
        :param description: String
        :param address: String
        :param website: String
        :param phone: String
        :return: True
        :raises: Database errors on connection and insertion
        """
        params = {'name':name, 'description':description, 'address':address, 'website':website, 'phone':phone}
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("insert into charity (name, description, address, website, phone) VALUES (:name, :description, :address, :website, :phone)", params)

        connection.commit()
        cursor.close()
        return True
